
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 17);

DELETE FROM qcril_emergency_source_mcc_table  where MCC = '424';
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','112','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','116111','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','901','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','909','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','911','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','922','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','991','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','992','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','993','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','995','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','996','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','997','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','998','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('424','999','','');

DELETE FROM qcril_emergency_source_mcc_mnc_table  where MCC = '604' AND MNC = '00';
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','150','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','19','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','177','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','15','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','190','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','00','911','','');


DELETE FROM qcril_emergency_source_voice_mcc_mnc_table  where MCC = '604' AND MNC = '00';
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','150','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','19','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','177','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','112','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','15','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','190','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','00','911','','');

COMMIT TRANSACTION;

