
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 14);

DELETE FROM qcril_emergency_source_mcc_mnc_table  where MCC = '510' AND MNC = '01';

INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','110','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','113','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','115','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','118','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','119','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','129','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','911','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','01','123','','');

DELETE FROM qcril_emergency_source_mcc_mnc_table  where MCC = '510' AND MNC = '11';
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','110','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','113','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','115','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','118','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','119','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','129','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','911','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('510','11','117','','');

DELETE FROM qcril_emergency_source_voice_mcc_mnc_table  where MCC = '510' AND MNC = '01';
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('510','01','123','','');



COMMIT TRANSACTION;

