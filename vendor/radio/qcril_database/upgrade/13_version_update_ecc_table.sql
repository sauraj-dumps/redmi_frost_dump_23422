
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 13);

DELETE FROM qcril_emergency_source_voice_table where MCC = '457';
INSERT INTO "qcril_emergency_source_voice_table" VALUES('457','1190','','full');
INSERT INTO "qcril_emergency_source_voice_table" VALUES('457','1191','','full');
INSERT INTO "qcril_emergency_source_voice_table" VALUES('457','1195','','full');
INSERT INTO "qcril_emergency_source_voice_table" VALUES('457','1199','','full');


COMMIT TRANSACTION;

