
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 11);

DELETE FROM qcril_emergency_source_voice_mcc_mnc_table where MCC = '510';
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('510','09','110','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('510','10','110','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('510','11','110','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('510','28','110','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('510','89','110','','');

COMMIT TRANSACTION;

