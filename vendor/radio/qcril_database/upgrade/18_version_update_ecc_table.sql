
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 18);

DELETE FROM qcril_emergency_source_mcc_mnc_table  where MCC = '604' AND MNC = '01';
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','150','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','19','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','177','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','15','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','190','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','01','911','','');

DELETE FROM qcril_emergency_source_mcc_mnc_table  where MCC = '604' AND MNC = '02';
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','150','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','19','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','177','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','15','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','190','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('604','02','911','','');


DELETE FROM qcril_emergency_source_voice_mcc_mnc_table  where MCC = '604' AND MNC = '01';
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','150','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','19','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','177','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','112','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','15','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','190','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','01','911','','');

DELETE FROM qcril_emergency_source_voice_mcc_mnc_table  where MCC = '604' AND MNC = '02';
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','150','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','19','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','177','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','112','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','15','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','190','','');
INSERT INTO "qcril_emergency_source_voice_mcc_mnc_table" VALUES('604','02','911','','');

DELETE FROM qcril_emergency_source_mcc_table  where MCC = '621';
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('621','119','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('621','767','','');

COMMIT TRANSACTION;

