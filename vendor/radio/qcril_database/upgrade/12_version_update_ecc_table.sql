
/*
  Copyright (c) 2019 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
BEGIN TRANSACTION;
INSERT OR REPLACE INTO qcril_properties_table (property, value) VALUES ('qcrildb_version', 12);

DELETE FROM qcril_emergency_source_hard_mcc_table where MCC = '714';
INSERT INTO "qcril_emergency_source_hard_mcc_table" VALUES('714','112','','');
INSERT INTO "qcril_emergency_source_hard_mcc_table" VALUES('714','911','','');
INSERT INTO "qcril_emergency_source_hard_mcc_table" VALUES('714','104','','');	

DELETE FROM qcril_emergency_source_mcc_mnc_table where MCC = '714';
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','02','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','02','911','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','103','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','104','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','105','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','106','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','107','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','108','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','109','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','112','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','133','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','141','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','155','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','311','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','03','911','','');
INSERT INTO "qcril_emergency_source_mcc_mnc_table" VALUES('714','04','109','','');

DELETE FROM qcril_emergency_source_mcc_table where MCC = '457';
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','110','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','191','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','192','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','199','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','1190','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','1191','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','1195','','');
INSERT INTO "qcril_emergency_source_mcc_table" VALUES('457','1199','','');
COMMIT TRANSACTION;

