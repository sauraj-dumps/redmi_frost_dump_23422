#! /vendor/bin/sh
export PATH=/vendor/bin

if [ -f /sys/class/drm/card0-DSI-1/modes ]; then
    echo "detect" > /sys/class/drm/card0-DSI-1/status
    mode_file=/sys/class/drm/card0-DSI-1/modes
    while read line; do
        fb_width=${line%%x*};
        break;
    done < $mode_file
fi

function set_density_by_fb() {
    #put default density based on width
    if [ -z $fb_width ]; then
        setprop vendor.display.lcd_density 320
    else
        if [ $fb_width -ge 1600 ]; then
           setprop vendor.display.lcd_density 640
        elif [ $fb_width -ge 1440 ]; then
           setprop vendor.display.lcd_density 560
        elif [ $fb_width -ge 1080 ]; then
           setprop vendor.display.lcd_density 480
        elif [ $fb_width -ge 720 ]; then
           setprop vendor.display.lcd_density 320 #for 720X1600 resolution
        elif [ $fb_width -ge 480 ]; then
            setprop vendor.display.lcd_density 240 #for 480X854 QRD resolution
        else
            setprop vendor.display.lcd_density 160
        fi
    fi
}

target=`getprop ro.board.platform`
case "$target" in
    "jr510")
        setprop vendor.display.lcd_density 320
        ;;
esac

#set default lcd density
#Since lcd density has read only
#property, it will not overwrite previous set
#property if any target is setting forcefully.
set_density_by_fb

boot_reason=`cat /proc/sys/kernel/boot_reason`
reboot_reason=`getprop ro.boot.alarmboot`
if [ "$boot_reason" = "3" ] || [ "$reboot_reason" = "true" ]; then
    setprop ro.vendor.alarm_boot true
else
    setprop ro.vendor.alarm_boot false
fi

diag_extra=`getprop persist.vendor.usb.config.extra`
if [ "$diag_extra" == "" ]; then
    setprop persist.vendor.usb.config.extra none
fi
