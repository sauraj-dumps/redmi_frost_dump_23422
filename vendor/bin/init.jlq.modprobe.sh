#! /vendor/bin/sh
export PATH=/vendor/bin

boot_mode=`getprop ro.bootmode`
#both for noraml mode and charger mode
#/vendor/bin/modprobe -a -d /vendor/lib/modules
if [ "$boot_mode" != "charger" ]; then
    /vendor/bin/modprobe -a -d /vendor/lib/modules mali_kbase_dlkm mve_resource mve_base vipcore camera.ko jlq_cam_thermal.ko rmnet_ctl.ko rmnet_core.ko
    if [ $? -ne 0 ];then
        /vendor/bin/modprobe -a -d /vendor/lib/modules/5.4-gki mali_kbase_dlkm mve_resource mve_base vipcore camera.ko jlq_cam_thermal.ko rmnet_ctl.ko rmnet_core.ko
    fi
fi
setprop ro.vendor.modprobe true
