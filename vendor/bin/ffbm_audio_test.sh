#!/vendor/bin/sh

# FFBM_ECHO="echo"
FFBM_ECHO="log -p d -t ffbm_audio_test_sh"

enable=1
disable=0

playback_mode=0
playback_handset=1
playback_headset=2
playback_speaker=3

file_path_default="/data/vendor/audio/amt.wav"
duration_time_default=3
rx_vol_default=84
rx_spk_default=80
rx_handset_default=87


if [ ! -f "$file_path_default" ]
then
	cp -f /vendor/etc/amt.wav "$file_path_default"
	$FFBM_ECHO "copy from /vendor/etc/amt.wav"
fi


$FFBM_ECHO "ffbm_audio_test in"

### check parameter number ###
if [ $# -lt 5 ]
then
	$FFBM_ECHO "please input command as: case_id [file] [time] [volume] [start/stop]"
	$FFBM_ECHO "example: ffbm_audio_test.sh speaker_playback_test /data/vendor/audio/amt.wav 5 84 1"
	exit 1
fi

pidcount=`ps -ef|grep ffbm_audio_test |grep -v grep|grep -v $0|wc -l`
if [ $pidcount -ne 0 ];then
	$FFBM_ECHO "pidcount: $pidcount"
	if [[ $# -eq 4 ]] || ([[ $# -ge 5 ]] && [[ "$5" != 0 ]]);then
		$FFBM_ECHO "Already running process,new process will not run"
		exit 2
	fi
fi


if [ ! -f "$2" ];then
	file_path=$file_path_default
	$FFBM_ECHO "file_path: $2 not exists,set $file_path"
else
	file_path=$2
	$FFBM_ECHO "file_path: $file_path"
fi

duration_time=$duration_time_default
if [ -n "$3" ]
then
	duration_time=$3
fi

if ([[ $4 -lt 0 ]] || [[ $4 -gt 124 ]]);then
	rx_vol=$rx_vol_default
	$FFBM_ECHO "rx_vol: $4 out range,set default"
else
	rx_vol=$4
fi

case $1 in
	handset_playback_test)
		playback_mode=$playback_handset
		rx_vol=$rx_handset_default
	;;
	headset_playback_test)
		playback_mode=$playback_headset
	;;
	speaker_playback_test)
		playback_mode=$playback_speaker
		rx_vol=$rx_spk_default
	;;
	*)
	;;
esac


var_all="$1 $file_path $duration_time $rx_vol $5"
$FFBM_ECHO "ffbm_audio_test param: $var_all"

setprop persist.vendor.audio.ffbmaudiotest "$var_all"

if test $5 -eq $enable
then
	if test $playback_mode -ne 0
	then
		$FFBM_ECHO "enable playback_mode: $playback_mode"
		ffbm_audio_test $file_path -D 0 -d 0 -tc $playback_mode -t $duration_time -v $rx_vol
		ps -ef|grep ffbm_audio_test |grep -v grep|grep -v $0
		if [ $? -eq 0 ]
		then
			pkill -15 ffbm_audio_test
		fi
	else
		$FFBM_ECHO "ffbm-loopback start!"
		start ffbm-loopback
	fi

elif test $5 -eq $disable
then
	if test $playback_mode -ne 0
	then
		$FFBM_ECHO "disable playback_mode: $playback_mode"
		ps -ef|grep ffbm_audio_test |grep -v grep|grep -v $0
		if [ $? -eq 0 ]
		then
			pkill -15 ffbm_audio_test
		fi
	else
		ps -ef|grep ffbm_audio_test |grep -v grep|grep -v $0
		if [ $? -eq 0 ]
		then
			$FFBM_ECHO "Ready to term running process"
			##pkill -15 ffbm_audio_test
			##stop ffbm-loopback
			setprop persist.vendor.ffbm.audio.state "term"
			while :
			do
				audio_state=`getprop persist.vendor.ffbm.audio.state`
				if [ "$audio_state" != 'term' ]
				then
					$FFBM_ECHO "audio_state = $audio_state"
					break
				fi
			done
		else
			$FFBM_ECHO "no running process,no need close"
		fi
	fi
else
	$FFBM_ECHO "input error ctl cmd!"
fi


$FFBM_ECHO "ffbm_audio_test out"

exit 0