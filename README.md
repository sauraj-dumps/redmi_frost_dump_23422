## frost-user 11 RD2A.211001.002 V13.0.10.0.RGFIDXM release-keys
- Manufacturer: xiaomi
- Platform: jr510
- Codename: frost
- Brand: Redmi
- Flavor: frost-user
frost-user
frost-user
frost-user
frost-user
- Release Version: 11
- Kernel Version: 5.4.191
- Id: RD2A.211001.002
- Incremental: V13.0.10.0.RGFIDXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/frost/frost:11/RD2A.211001.002/V13.0.10.0.RGFIDXM:user/release-keys
- OTA version: 
- Branch: frost-user-11-RD2A.211001.002-V13.0.10.0.RGFIDXM-release-keys
- Repo: redmi_frost_dump_23422
