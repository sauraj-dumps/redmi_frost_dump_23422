#! /vendor/bin/sh

# Copyright (c) 2012-2013, 2016-2020, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of The Linux Foundation nor
#       the names of its contributors may be used to endorse or promote
#       products derived from this software without specific prior written
#       permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Parse misc partition path and set property

function zram_parameters_config()
{
    MemTotalStr=`cat /proc/meminfo | grep MemTotal`
    MemTotal=${MemTotalStr:16:8}

    low_ram=`getprop ro.config.low_ram`
    let RamSizeGB="( $MemTotal / 1048576 ) + 1"
    diskSizeUnit=M
    if [ $RamSizeGB -le 3 ]; then
        let zRamSizeMB="( $RamSizeGB * 1024 ) * 3 / 4"
    else
        let zRamSizeMB="( $RamSizeGB * 1024 ) / 2"
    fi

    # use MB avoid 32 bit overflow
    if [ $zRamSizeMB -gt 4096 ]; then
        let zRamSizeMB=4096
    fi

    #if [ "$low_ram" == "true" ]; then
        echo "set algo"
        echo lz4 > /sys/block/zram0/comp_algorithm
    #fi

    if [ -f /sys/block/zram0/disksize ]; then
        if [ -f /sys/block/zram0/use_dedup ]; then
            echo 1 > /sys/block/zram0/use_dedup
        fi

        echo "$zRamSizeMB""$diskSizeUnit" > /sys/block/zram0/disksize

        # ZRAM may use more memory than it saves if SLAB_STORE_USER
        # debug option is enabled.
        if [ -e /sys/kernel/slab/zs_handle ]; then
            echo 0 > /sys/kernel/slab/zs_handle/store_user
        fi
        if [ -e /sys/kernel/slab/zspage ]; then
            echo 0 > /sys/kernel/slab/zspage/store_user
        fi
        #let extraMem="$RamSizeGB * 1048576 / 96"
        #setprop vendor.sysctl.extra_free_kbytes $extraMem
        echo 1 > /proc/sys/vm/watermark_scale_factor
        echo "512 32 0" > /proc/sys/vm/lowmem_reserve_ratio

        mkswap /dev/block/zram0
        swapon /dev/block/zram0 -p 32758
    fi
}

function swap_on_data()
{
    MemTotalStr=`cat /proc/meminfo | grep MemTotal`
    MemTotal=${MemTotalStr:16:8}

    SWAP_ENABLE_THRESHOLD=1048576
    swap_enable=`getprop ro.vendor.jlq.swapondata.enable`

    # Enable swap initially only for 1 GB targets
    if [ "$MemTotal" -le "$SWAP_ENABLE_THRESHOLD" ] && [ "$swap_enable" == "true" ]; then
        # Static swiftness
        echo 1 > /proc/sys/vm/swap_ratio_enable
        echo 70 > /proc/sys/vm/swap_ratio

        # Swap disk - 200MB size
        if [ ! -f /data/vendor/swap/swapfile ]; then
            dd if=/dev/zero of=/data/vendor/swap/swapfile bs=1m count=200
        fi
        mkswap /data/vendor/swap/swapfile
        swapon /data/vendor/swap/swapfile -p 32758
    fi
}

misc_link=$(ls -l /dev/block/bootdevice/by-name/misc)
real_path=${misc_link##*>}
setprop persist.vendor.mmi.misc_dev_path $real_path

# cpuset parameters
# move low priority task to silver cluster
echo 4-7 > /dev/cpuset/background/cpus
echo 4-7 > /dev/cpuset/system-background/cpus
echo 0-3 > /dev/cpuset/display-service/cpus

# Turn off scheduler boost at the end
# TODO: tuning uclamp parameters
echo 0  > /dev/cpuctl/rt/cpu.uclamp.latency_sensitive
echo 2.00 > /dev/cpuctl/rt/cpu.uclamp.min
echo 200000 > /proc/sys/kernel/sched_migration_cost_ns
echo 0  > /dev/cpuctl/top-app/cpu.uclamp.latency_sensitive
echo 10.00 > /dev/cpuctl/top-app/cpu.uclamp.min
echo 0  > /dev/cpuctl/foreground/cpu.uclamp.latency_sensitive
echo 1.00 > /dev/cpuctl/foreground/cpu.uclamp.min
echo 1  > /dev/cpuctl/display-service/cpu.uclamp.latency_sensitive
echo 10.00 > /dev/cpuctl/display-service/cpu.uclamp.min
echo 0  > /dev/cpuctl/background/cpu.uclamp.latency_sensitive
echo 0.00 > /dev/cpuctl/background/cpu.uclamp.min
echo 1.00 > /dev/cpuctl/nnapi-hal/cpu.uclamp.min

#TODO: tuning blkio group parameters
echo 200 > /dev/blkio/background/blkio.bfq.weight
echo 200 > /dev/blkio/forefround/blkio.bfq.weight

# Enable Zram Config
zram_parameters_config
echo 0 > /proc/sys/vm/page-cluster
echo 100 > /proc/sys/vm/swappiness
# Enable Zram Config End

#set ddr freq governor
echo "userspace" > /sys/devices/platform/0.soc/0.soc:jlq,ddr-devfreq/devfreq/0.soc:jlq,ddr-devfreq/governor

#set cpu freq governor as schedutil
echo "schedutil" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo "schedutil" > /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor

echo "17000000 1 0" > /sys/devices/platform/0.soc/30400000.gpu/pm_poweroff
#performance add
echo 1  > /dev/cpuctl/camera-daemon/cpu.uclamp.latency_sensitive
echo 1.00 > /dev/cpuctl/camera-daemon/cpu.uclamp.min
echo 200000 > /proc/sys/kernel/sched_migration_cost_ns
