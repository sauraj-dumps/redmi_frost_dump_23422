#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_frost.mk

COMMON_LUNCH_CHOICES := \
    lineage_frost-user \
    lineage_frost-userdebug \
    lineage_frost-eng
