#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from frost device
$(call inherit-product, device/xiaomi/frost/device.mk)

PRODUCT_DEVICE := frost
PRODUCT_NAME := lineage_frost
PRODUCT_BRAND := Redmi
PRODUCT_MODEL := frost
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="frost-user 11 RD2A.211001.002 V13.0.10.0.RGFIDXM release-keys"

BUILD_FINGERPRINT := Redmi/frost/frost:11/RD2A.211001.002/V13.0.10.0.RGFIDXM:user/release-keys
